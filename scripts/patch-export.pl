#!/usr/bin/perl
use 5.030;
use feature 'signatures';
no warnings 'experimental::signatures';

# Reads a file into a scalar and returns it
sub slurp ($file) {
    open( my $in, '<', $file ) or die("Failed to open $file for reading: $!");
    local $/ = undef;
    my $content = <$in>;
    close($in);
    return $content;
}

sub main () {
    my $file = shift(@ARGV);
    if ( !defined $file || !-e $file || !-w $file ) {
        die(
"Usage: $0 file.html\n\nWill patch file.html and replace the javascript with ./assets/web.js\nUseful for testing updates to the Migraine Log JS component\n"
        );
    }
    my $newJS = slurp('./assets/web.js');

    my $patchFile = slurp($file);
    my @out;
    my $seenScript  = 0;
    my $scriptEnded = 0;
    my $jsOpening;
    foreach my $line ( split( "\n", $patchFile ) ) {
        chomp($line);
        if ( $line =~ /^<script.+javascript.+/ ) {
            $seenScript = 1;
            $jsOpening  = $line;
            $jsOpening =~ s/>.+/>/;
        }
        if ($seenScript) {
            if ( !$scriptEnded ) {
                if ( $line =~ /^<\/script/ ) {
                    $scriptEnded = 1;
                    push( @out, $jsOpening . $newJS );
                    push( @out, $line );
                }
            }
            next;
        }
        push( @out, $line );
    }

    open( my $outFile, '>', $file );
    print {$outFile} join( "\n", @out );
}

main();
