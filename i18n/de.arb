{
  "@@last_modified": "2025-01-19T19:53:11.738830",
  "@@locale": "de",
  "General": "Allgemein",
  "@General": {
    "description": "Used as a header in the config screen to refer to \"general settings\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Continue": "Weiter",
  "@Continue": {
    "description": "Used as a button in the 'first time' configuration screen",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Use neutral language": "Neutralere Sprache",
  "@Use neutral language": {
    "description": "Toggle button that enables (or disables) use of \"headache\" instead of \"migraine\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Use the term \"headache\" rather than \"migraine\"": "Verwende den Begriff \"Kopfschmerz\" anstelle von \"Migräne\"",
  "@Use the term \"headache\" rather than \"migraine\"": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "What kind of headache do you have?": "Welche Art von Kopfschmerz hast Du?",
  "@What kind of headache do you have?": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Select this if you're unsure": "Wähle diese Option, wenn Du Dir nicht sicher bist",
  "@Select this if you're unsure": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine": "Migräne",
  "@Migraine": {
    "description": "Used as level 2/3 (medium) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Other (general headaches, cluster etc.)": "Andere Kopfschmerzart (allgemein, Cluster, etc.)",
  "@Other (general headaches, cluster etc.)": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Settings": "Einstellungen",
  "@Settings": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Configuration": "Einrichtung",
  "@Configuration": {
    "description": "The name of the first time configuration screen. This is pretty much like the settings screen, but with some different information and displayed only once, therefore using a different name to make this clearer.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medication list": "Medikamente",
  "@Medication list": {
    "description": "Header for the part of the configuration screen where the user sets up their medication",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Changing the list of medications here will not change medications in entries you have already added.": "Wenn Du einzelne Medikamente hier änderst oder löschst, hat das keinen Einfluss auf Medikamente in Einträgen, die Du bereits gespeichert hast.",
  "@Changing the list of medications here will not change medications in entries you have already added.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.": "Wenn Du (noch) keine Medikamente hinzufügen möchtest, kannst Du auf \"Weiter\" klicken und Migräne Logbuch wird zwei allgemein verwendbare Kategorien für Dich erzeugen, die Du dann verwenden kannst.",
  "@If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Add a new medication": "Füge ein neues Medikament hinzu",
  "@Add a new medication": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Undo": "Rückgängig",
  "@Undo": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "_deletedElement": "\"{medication}\" gelöscht",
  "@_deletedElement": {
    "description": "A pop-up message after the user has deleted a medication. Is accompanied with an 'undo' button.",
    "type": "text",
    "placeholders_order": [
      "medication"
    ],
    "placeholders": {
      "medication": {}
    }
  },
  "No registration": "Keine Einträge",
  "@No registration": {
    "description": "Used in the pie diagram and table on the front page to denote days where no entries have been added. Avoid terminology like 'no headaches', since that might not be quite correct.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Headache": "Kopfschmerz",
  "@Headache": {
    "description": "Used as level 1/3 (lowest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Mild headache": "Leichter Kopfschmerz",
  "@Mild headache": {
    "description": "Used as neutral level 1/3 (lowest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Moderate headache": "Mittlerer Kopfschmerz",
  "@Moderate headache": {
    "description": "Used as neutral level 2/3 (medium) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Strong migraine": "Starke Migräne",
  "@Strong migraine": {
    "description": "Used as level 3/3 (strongest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Strong headache": "Starker Kopfschmerz",
  "@Strong headache": {
    "description": "Used as neutral level 3/3 (strongest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine medication": "Migräne-Medikament",
  "@Migraine medication": {
    "description": "Used as a generic 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Seizure medication": "Sedierendes Medikament",
  "@Seizure medication": {
    "description": "Used as neutral generic 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Painkillers": "Schmerzmittel",
  "@Painkillers": {
    "description": "Used as the second 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Edit": "Bearbeiten",
  "@Edit": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Register": "Eintrag erstellen",
  "@Register": {
    "description": "Displayed as the header when registering a new migraine attack",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "You must select a strength": "Du musst die Stärke der Attacke angeben",
  "@You must select a strength": {
    "description": "Notice that appears if the user tries to save an entry without selecting a strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Save": "Speichern",
  "@Save": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "editingEntryNewDate": "Du bearbeitest gerade den Eintrag von {editDate}. Wenn Du das Datum änderst, wird dieser Eintrag auf das neue Datum verschoben.",
  "@editingEntryNewDate": {
    "type": "text",
    "placeholders_order": [
      "editDate"
    ],
    "placeholders": {
      "editDate": {}
    }
  },
  "Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.": "Achtung: Für dieses Datum hast Du bereits einen Eintrag erstellt. Wenn Du speicherst, wird der vorhandene Eintrag überschrieben.",
  "@Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Date": "Datum",
  "@Date": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "change date": "Datum ändern",
  "@change date": {
    "description": "Used as an action label on the button that triggers a calendar to select the date for an attack when a screen reader is used. It will be contextualized by the OS, on Android the OS will say the label of the button, then the word 'button', followed by 'double tap to change date'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Strength": "Stärke der Attacke",
  "@Strength": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.": "Schmerzen nimmt jeder Mensch anders wahr. Die einzige Person, die einschätzen kann, wie stark Deine Attacke ist, bist Du selbst. Falls Du Dir unsicher bist, wähle das höhere Schmerzlevel.",
  "@Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Unable to perform most activities": "Nicht in der Lage alltägliche Dinge zu erledigen",
  "@Unable to perform most activities": {
    "description": "Migraine strength 3/3",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Unable to perform some activities": "In einigen alltäglichen Dingen eingeschränkt",
  "@Unable to perform some activities": {
    "description": "Migraine strength 2/3",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Able to perform most activities": "In der Lage die meisten Dinge zu erledigen",
  "@Able to perform most activities": {
    "description": "Migraine strength 1/3",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Close": "Schließen",
  "@Close": {
    "description": "Used as a 'close' button in the help dialog for migraine strength. This will be converted to upper case when used in this context, ie. 'Close' becomes 'CLOSE'. This to fit with Android UI guidelines",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Which strength should I choose?": "Welches Schmerzlevel soll ich wählen?",
  "@Which strength should I choose?": {
    "description": "Used as the tooltip for the help button in the 'add' and 'edit' screens as well as the title of the help window that pops up when this button is pressed.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medications taken": "Eingenommene Medikamente",
  "@Medications taken": {
    "description": "Header in the add/edit window",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Note": "Notizen",
  "@Note": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "(none)": "(keine)",
  "@(none)": {
    "description": "Used in exported HTML when the user has taken no medications. It is an entry in a table where the header is 'Medications'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medications": "Medikamente",
  "@Medications": {
    "description": "Used as a table header in exported data",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine Log": "Migräne Logbuch",
  "@Migraine Log": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "hide": "minimieren",
  "@hide": {
    "description": "Used in exported HTML as a link that hides one month. Will be displayed like '[hide]', so it should be lower case unless there are good reasons not to",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Hide this month": "Monat ausblenden",
  "@Hide this month": {
    "description": "Tooltip for a link that hides a month from view in the exported HTML",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "hidden:": "minimiert:",
  "@hidden:": {
    "description": "Will be rendered like: \"hidden: january 2021\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Total headache days": "Schmerztage insgesamt",
  "@Total headache days": {
    "description": "Used in a table, will render like 'Total headache days    20 days'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Taken medication": "Tage mit Medikamenteneinnahme",
  "@Taken medication": {
    "description": "Used in a table, will render like 'Taken Medication    5 days'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Filter:": "Filter:",
  "@Filter:": {
    "description": "Will be joined with the string 'show' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "show": "zeige",
  "@show": {
    "description": "Will be joined with the string 'Filter:' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "everything": "alles",
  "@everything": {
    "description": "Will be joined with the string 'Filter: show' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "months": "Monate",
  "@months": {
    "description": "Will be joined with a number, which will be 3 or higher, to become ie. '3 months'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "_generatedMessage": "Erstellt von Migräne Logbuch {version}",
  "@_generatedMessage": {
    "type": "text",
    "placeholders_order": [
      "version"
    ],
    "placeholders": {
      "version": {}
    }
  },
  "Loading...": "Lade Daten...",
  "@Loading...": {
    "description": "Used in exported HTML to indicate that we're loading the exported data",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Previous month": "Vorheriger Monat",
  "@Previous month": {
    "description": "Tooltip for the previous month button",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Next month": "Nächster Monat",
  "@Next month": {
    "description": "Tooltip for the next month button",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "_currentMonth": "Aktueller Monat: {month}",
  "@_currentMonth": {
    "description": "Used for screen readers to describe the element in the month selector that indicates the current month. The month variable will contain a pre-localized month name along with the year",
    "type": "text",
    "placeholders_order": [
      "month"
    ],
    "placeholders": {
      "month": {}
    }
  },
  "Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the \"+\"-button.\n\nFor more help, select \"Help\" in the menu at the top right of the screen.": "Migräne Logbuch hilft Dir dabei, Dein persönliches Kopfschmerztagebuch zu führen. Sobald Du Deinen ersten Eintrag erstellt hast, werden hier Statistiken angezeigt.\n\nMit Hilfe des \"+\"-Buttons kannst Du einen neuen Eintrag erstellen.\n\nWeitere Hilfe bekommst Du jederzeit unter dem Punkt \"Hilfe\" im Menü oben rechts.",
  "@Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the \"+\"-button.\n\nFor more help, select \"Help\" in the menu at the top right of the screen.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Help": "Hilfe",
  "@Help": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Icons in the calendar": "Markierungen im Kalender",
  "@Icons in the calendar": {
    "description": "Header in the help screen",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Colours": "Farben",
  "@Colours": {
    "description": "Header in the help screen",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.": "An Tagen, die mit einen Unterstrich markiert sind, hast Du zwar einen Schmerzeintrag erstellt, aber keine Medikamente eingenommen.",
  "@An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.": "An Tagen, die mit einem Kreis markiert sind, hast Du einen Schmerzeintrag erstellt und auch Medikamente eingenommen.",
  "@A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Throughout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.": "Migräne Logbuch verwendet Farben, um die Stärke deiner Attacken visuell darzustellen. Sowohl der Unterstrich/Kreis im Kalender als auch die Stärke der Attacke, die Du beim Erstellen bzw. Ändern eines Eintrags angibst, erscheinen in der Farbe, die dem jeweiligen Schmerzlevel zugeordnet ist.",
  "@Throughout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "dayString": "{number,plural, =1{{number} Tag}=2{{number} Tage}other{{number} Tage}}",
  "@dayString": {
    "description": "How many days there are. This isn't as complicated as it looks. Unless you know you need to modify the pluralization rules (in which case see https://pub.dev/documentation/intl/latest/intl/Intl/plural.html) you should just change the string 'day' and 'days' here into your local equivalent. For instance, the Norwegian version of this string is: '{number,plural, =1{{number} dag}=2{{number} dagar}other{{number} dagar}}'. number will always be a positive integer (never zero).",
    "type": "text",
    "placeholders_order": [
      "number"
    ],
    "placeholders": {
      "number": {}
    }
  },
  "Successfully imported data": "Datei erfolreich importiert",
  "@Successfully imported data": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "The file is corrupt and can not be imported": "Die Datei ist fehlerhaft und kann nicht importiert werden",
  "@The file is corrupt and can not be imported": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "This file does not appear to be a Migraine Log (html) file": "Die Datei scheint keine von Migräne Logbuch exportierte (HTML-)Datei zu sein",
  "@This file does not appear to be a Migraine Log (html) file": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "This file is from a newer version of Migraine Log. Upgrade the app first.": "Die Datei wurde mit einer neueren Version von Migräne Logbuch erstellt. Sie kann erst nach einem Update der App importiert werden.",
  "@This file is from a newer version of Migraine Log. Upgrade the app first.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "An unknown error occurred during import.": "Während des Imports ist ein unbekannter Fehler aufgetreten.",
  "@An unknown error occurred during import.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Import failed:": "Import fehlgeschlagen:",
  "@Import failed:": {
    "description": "Will contain information about why after the :",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.": "Deine Version von Migräne Logbuch ist zu alt, um die Daten einzulesen. Bitte update die App.",
  "@Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.": "Migräne Logbuch ist freie Software. Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation veröffentlicht, weitergeben und/oder modifizieren, entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.\n\nDie Veröffentlichung dieses Programms erfolgt in der Hoffnung, daß es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, sogar ohne die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.",
  "@Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Copyright ©": "Copyright ©",
  "@Copyright ©": {
    "description": "Will be followed by the author name and copyright year, which will be a link",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "See the": "Details finden Sie in der",
  "@See the": {
    "description": "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "for details.": ".",
  "@for details.": {
    "description": "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "About": "Über",
  "@About": {
    "description": "Menu entry for triggering the about dialog",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Exit": "Schließen",
  "@Exit": {
    "description": "Used to exit the app",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Export": "Exportieren",
  "@Export": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Import": "Importieren",
  "@Import": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Statistics": "Statistiken",
  "@Statistics": {
    "description": "Tooltip for the statistics tab",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Calendar": "Kalender",
  "@Calendar": {
    "description": "Tooltip for the calendar tab",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Home": "Home",
  "@Home": {
    "description": "Tooltip for the home tab",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Translated by": "Übersetzt von",
  "@Translated by": {
    "description": "Should be a literal translation of this phrase, the value of TRANSLATED_BY will be appended to make a string like this: \"Translated by: TRANSLATED_BY\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "TRANSLATED_BY": "jofrev",
  "@TRANSLATED_BY": {
    "description": "Should be an alphabetical list of the names of the translators of this language, delimited with commas or the localized equivalent of \"and\". Will be displayed in the about dialog like this: \"Translated by: TRANSLATED_BY\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "deletedDateMessage": "{fmtDate} gelöscht",
  "@deletedDateMessage": {
    "type": "text",
    "placeholders_order": [
      "fmtDate"
    ],
    "placeholders": {
      "fmtDate": {}
    }
  },
  "Delete": "Löschen",
  "@Delete": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Add": "Neu",
  "@Add": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Show action menu": "Action-Menü einblenden",
  "@Show action menu": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "lastNDaysMessage": "Letzten {days} Tage",
  "@lastNDaysMessage": {
    "description": "days should always be >1, so plural",
    "type": "text",
    "placeholders_order": [
      "days"
    ],
    "placeholders": {
      "days": {}
    }
  },
  "took no medication": "keine Medikamente genommen",
  "@took no medication": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "tookMedsMessage": "behandelt mit {meds}",
  "@tookMedsMessage": {
    "description": "This will expand into a list of medications taken, and will be added to the type of headache the user had. For instance, this might get 'Imigran' as meds, this will then be 'took Imigran', and it might expand into the sentece 'Migraine, took Imigran'",
    "type": "text",
    "placeholders_order": [
      "meds"
    ],
    "placeholders": {
      "meds": {}
    }
  },
  "Note:": "Notiz:",
  "@Note:": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Show note indicators": "Notizen hervorheben",
  "@Show note indicators": {
    "description": "Toggle button that enables/disables showing a marker on dates with notes in the calendar",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Show a small dot on dates where you have written a note": "Markiere Tage mit einer Notiz mit einem kleinen Punkt im Kalender",
  "@Show a small dot on dates where you have written a note": {
    "description": "The 'dot' is a small grey square, displayed in the upper right hand corner of the date number in the calendar",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "List days with no entries": "Zeige Tage ohne Einträge",
  "@List days with no entries": {
    "description": "Checkbox, toggles showing or hiding (default=hide) dates that have no registered headaches in the tables in our exported HTML file",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "A dot in the upper right hand corner of a date indicates that the entry on that date includes a note.": "Ein Punkt in der rechten oberen Ecke eines Tages zeigt an, dass der entsprechende Eintrag eine Notiz enthält.",
  "@A dot in the upper right hand corner of a date indicates that the entry on that date includes a note.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Aura only": "Nur Aura",
  "@Aura only": {
    "description": "Used as the 'strength' of a migraine or headache where the user only had aura symptoms",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Aura symptoms only": "Keine Schmerzen sondern nur Aura Symptome",
  "@Aura symptoms only": {
    "description": "Migraine strength help text for aura only",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Let me set \"aura only\" as a the strength of an attack": "Ermöglicht \"Nur Aura\" als Stärke einer Attacke anzugeben",
  "@Let me set \"aura only\" as a the strength of an attack": {
    "description": "This is used as a description for the 'aura only' config option. Feel free to rephrase.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Enable \"aura only\" strength": "Biete \"Nur Aura\" als zusätzliches Schmerzlevel an",
  "@Enable \"aura only\" strength": {
    "description": "Toggle button that enables (or disables) use of \"aura only\" as a \"strength\" option",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Your settings file failed to load. Your data is safe, and you may reset your settings to fix this issue (you will need to enter your medications again).": "Fehler beim Laden der Einstellungen. Du verlierst keine Daten und kannst den Fehler beheben, indem Du Deine Einstellungen zurücksetzt (Du musst nur Deine Medikamente erneut anlegen).",
  "@Your settings file failed to load. Your data is safe, and you may reset your settings to fix this issue (you will need to enter your medications again).": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "If you want to report this issue, take a screenshot of the following information:": "Bitte mache einen Screenshot von den folgenden Daten, wenn Du diesen Fehler melden möchtest:",
  "@If you want to report this issue, take a screenshot of the following information:": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Reset settings": "Einstellungen zurücksetzen",
  "@Reset settings": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Error": "Fehler",
  "@Error": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Enable notes on days without attacks": "Notizen an symptomfreien Tagen",
  "@Enable notes on days without attacks": {
    "description": "Toggle button that enables (or disables) use of \"no headache\" as a \"strength\" option",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Lets you choose a \"no headache\" as the strength of an attack": "Ermöglicht \"Symptomfrei\" als Stärke einer Attacke anzugeben",
  "@Lets you choose a \"no headache\" as the strength of an attack": {
    "description": "This is used as a description for the 'no headache' config option. Feel free to rephrase.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "No headache": "Symptomfrei",
  "@No headache": {
    "description": "Used as the 'strength' of a day where the user had no headache (for registering notes on days without symptoms)",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Register medications": "Register medications",
  "@Register medications": {
    "description": "Header in the add/edit window",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medication": "Medication",
  "@Medication": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Taken": "Taken",
  "@Taken": {
    "description": "This is used in the header of the 'list of medications' table in the viewer. The column it refers to will contain the time that a medication was taken",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "takenAt": "Took {medication} at {time}",
  "@takenAt": {
    "description": "medication is the name of one of the users medications, time is a time in either 24H or 12H format, depending on locale",
    "type": "text",
    "placeholders_order": [
      "medication",
      "time"
    ],
    "placeholders": {
      "medication": {},
      "time": {}
    }
  }
}