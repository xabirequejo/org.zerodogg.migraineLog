import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

Directory? overrideMLConfigDirectory;

Future<Directory> getApplicationStorageDirectory() async {
  if (overrideMLConfigDirectory != null) {
    return overrideMLConfigDirectory!;
  }
  // On Linux use the XDG base dir specification (which path_provider doesn't
  // really do). We inline our own implementation rather than using the
  // xdg_directories package since dart isn't great at platform specific
  // imports that are written in dart.
  if (Platform.isLinux) {
    String? home = Platform.environment['HOME'];
    String? xdg_config_home = Platform.environment['XDG_CONFIG_HOME'];
    // Dart doesn't provide any good way to find HOME other than the env, so we
    // need HOME (or XDG_CONFIG_HOME) to be set
    if (home == null) {
      if (xdg_config_home == null) {
        throw ("HOME is not set, unable to detect home dir");
      }
    } else {
      xdg_config_home ??= home + '/.config';
    }
    final path = xdg_config_home + '/migrainelog';
    final dir = Directory(path);
    await dir.create(recursive: true);
    return dir;
  } else {
    return await getApplicationDocumentsDirectory();
  }
}

void setWindowTitle(String title) {
  if (defaultTargetPlatform == TargetPlatform.linux) {
    WidgetsFlutterBinding.ensureInitialized();
    // We use a custom MethodChannel to implement setting the window title on Linux.
    // Under the hood this calls either gtk_window_set_title() or
    // gtk_header_bar_set_title(). See windowsettings_call_handler in
    // linux/my_application.cc
    const platform = MethodChannel('migrainelog.windowsettings');
    platform.invokeMethod("setWindowTitle", title);
  }
}
