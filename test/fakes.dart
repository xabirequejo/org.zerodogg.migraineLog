// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2022    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:mocktail/mocktail.dart';
import 'package:MigraineLog/datatypes.dart';
import 'package:flutter/material.dart';

class FakeMigraineMedicationEntry extends Fake
    implements MigraineMedicationEntry {}

class FakeMigraineConfigMedicationEntry extends Fake
    implements MigraineConfigMedicationEntry {}

class FakeMigraineEntry extends Fake implements MigraineEntry {}

class FakeRoute extends Fake implements Route {}
