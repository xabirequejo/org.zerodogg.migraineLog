// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2024   Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:mocktail/mocktail.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';
import 'dart:io';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';

String? testPrefix;

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

extension WidgetTesterFlush on WidgetTester {
  Future<bool> flush() async {
    await pump(Duration(minutes: 10));
    return true;
  }
}

void setTestFile(String file) {
  testPrefix = file;
}

String dateString(DateTime input) {
  return [input.year, input.month, input.day].join('-');
}

/// This is a helper function that handles some of the boilerplate that our
/// widgets need. If only given one argument (a widget), it wraps that in a
/// MaterialApp with a Scaffold. Optionally you may also provide a list
/// `provide`, in which case it will add a MultiProvider using
/// ChangeNotifierProvider.value of each of the objects in that list.
Widget materialWidget(
  Widget widget, {
  NavigatorObserver? navObserver,
  Map<String, WidgetBuilder>? routes,
  Widget? fab,
}) {
  List<NavigatorObserver> navObs = [];
  routes ??= {};
  if (navObserver != null) {
    navObs.add(navObserver);
  }
  return MaterialApp(
    home: Scaffold(
      body: widget,
      floatingActionButton: fab,
    ),
    navigatorObservers: navObs,
    routes: routes,
  );
}

Widget materialWidgetBuilder(
  Widget Function(BuildContext) cb, {
  required List<SingleChildWidget> provide,
  Map<String, WidgetBuilder>? routes,
  NavigatorObserver? navObserver,
}) {
  return MultiProvider(
    providers: provide,
    builder: (context, __) => materialWidget(
      cb(context),
      navObserver: navObserver,
      routes: routes,
    ),
  );
}

Future<void> initScreenSize(WidgetTester tester) async {
  await tester.setScreenSize(width: 540, height: 760);
}

Future<void> goldenTest({
  required Type widgetType,
  required WidgetTester tester,
  String? name,
  Widget? widgetInstance,
}) async {
  if (testPrefix == null) {
    throw ("You need to call setTestFile() before using goldenTest()");
  }
  if (name == null && widgetInstance != null) {
    name = widgetInstance.runtimeType.toString();
  } else if (name == null) {
    throw ("You must provide either name or widgetInstance");
  }
  await initScreenSize(tester);
  if (widgetInstance != null) {
    await tester.pumpWidget(materialWidget(widgetInstance));
  }
  await tester.pumpAndSettle();
  await expectLater(find.byType(widgetType),
      matchesGoldenFile('./golden/' + testPrefix! + '.' + name + '.png'));
}

extension SetScreenSize on WidgetTester {
  Future<void> setScreenSize(
      {required double width,
      required double height,
      double pixelDensity = 1}) async {
    final size = Size(width, height);
    await binding.setSurfaceSize(size);
    view.physicalSize = size;
    view.devicePixelRatio = pixelDensity;
  }
}

class MockPathProviderPlatform extends Mock
    with MockPlatformInterfaceMixin
    implements PathProviderPlatform {
  MockPathProviderPlatform({this.dir});
  Directory? dir;

  @override
  Future<String> getApplicationDocumentsPath() async {
    return dir!.path;
  }
}
